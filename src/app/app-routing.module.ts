import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckBoxComponent } from './check-box/check-box.component';


const routes: Routes = [
  { path: 'component', component: CheckBoxComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
