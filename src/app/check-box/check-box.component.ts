import { Component } from '@angular/core';

@Component({
  selector: 'app-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.css']
})
export class CheckBoxComponent {
  selectable = true;
  removable = true;

  //  The names are saved here
  nameAdd: any = [];

  //  More options add here
  list: any = [
    //  'Name' is the key do not remove it
    {
      'name': 'nome1'
    },
    {
      'name': 'nome2'
    },
    {
      'name': 'nome3'
    },
  ];

  //  Remove options selected users
  remove(nome: any): void {
    let index = this.nameAdd.indexOf(nome);
    this.nameAdd.splice(index, 1);
  }

  //  Add new names or options
  select(event: any): void {
    let index = this.nameAdd.indexOf(event.target.value);

    if (index === -1) {
      this.nameAdd.push(event.target.value);
    }
    event.target.value = '';
  }
}
